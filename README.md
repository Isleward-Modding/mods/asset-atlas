# Asset Atlas
Stores references to spritesheets and assets, by names.
Facilitates cell to position and position to cell conversions.

## Documentation
See Isleward-Modding/ars-botanica for a much better example and practical use.

The Asset Atlas mod emits three events:

`asset-atlas:onGetSpritesheets(Array spritesheets)`

Add spritesheets to this array to load spritesheets.
Added spritesheets can be referenced from mappings, and will be loaded on the client.
A spritesheet needs an id, a width (in tiles), and a path for the client to load it from.

`asset-atlas:onGetMappings(Array mappings)`

Add mappings to this array to map assets.
Mappings can be looked up later for their cell or position.
All mappings need an id, a cell OR a position (the other one will be generated from the spritesheet), and a spritesheet name.

`asset-atlas:onAtlasReady(Object atlasMethods)`

Contains methods from the atlas to look up spritesheets and mappings.
atlasMethods contains five methods: cellToPosition, positionToCell, xyToCell, getAsset, and getSpritesheet.

cellToPosition(cell, sheet) converts a cell (number) and a spritesheet (must be registered with the Asset Atlas first) to a position (array of [x, y]).

positionToCell([x, y], sheet) does the same thing as cellToPosition but backwards (give it the position and it gives the cell number).

xyToCell(x, y, sheet) is the same as positionToCell but with the arguments laid out differently.

getAsset(name) looks up an asset registered by ANY mod from its name, and returns an object with the cell, spritesheet name, and position.

getSpritesheet(name) looks up a spritesheet registered by ANY mod from its name, and returns an object with the width (in tiles) of the spritesheet and the path to the spritesheet image.

### One Last Note
You could probably also add any other properties you want to the spritesheet or mapping and it would be saved and retrieved later.
