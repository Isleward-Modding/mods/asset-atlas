const LOG_EXCEPTIONS = true;

class UnknownAssetException {
	constructor (asset) {
		this.message = 'Asset Atlas: ' + asset + ' was accessed, but doesn\'t exist';
		this.asset = asset;
		if (LOG_EXCEPTIONS)
			console.log(this);
	}
}
class GenericConflictException {
	constructor (type, conflicted) {
		this.message = 'Asset Atlas: ' + type + ' conflict detected (' + conflicted + ')';
		this.type = type;
		this.conflicted = conflicted;
		if (LOG_EXCEPTIONS)
			console.log(this);
	}
}
class AssetMissingSpriteException {
	constructor (id) {
		this.message = 'Asset Atlas: Asset ' + id + ' is missing a cell or position';
		this.failedId = id;
		if (LOG_EXCEPTIONS)
			console.log(this);
	}
}
class AssetMissingSpritesheetException {
	constructor (id) {
		this.message = 'Asset Atlas: Asset ' + id + ' is missing a spritesheet';
		this.failedId = id;
		if (LOG_EXCEPTIONS)
			console.log(this);
	}
}
class SpritesheetMissingPathException {
	constructor (id) {
		this.message = 'Asset Atlas: Spritesheet ' + id + ' is missing a path to an image';
		this.failedId = id;
		if (LOG_EXCEPTIONS)
			console.log(this);
	}
}
class SpritesheetMissingWidthException {
	constructor (id) {
		this.message = 'Asset Atlas: Spritesheet ' + id + ' is missing a width (in tiles)';
		this.failedId = id;
		if (LOG_EXCEPTIONS)
			console.log(this);
	}
}

module.exports = {
	name: 'Asset Atlas',

	spritesheets: null,
	mappings: null,

	init: function () {
		if (!!process.send) {
			// On map threads, use components because it is fired right after all mods are loaded
			// TODO: change to onAfterModsLoaded event, once that's a thing
			this.events.on('onBeforeGetComponents', this.onBeforeGetComponents.bind(this));
		} else {
			// Load the correct way on main thread
			this.events.on('onBeforeGetResourceList', this.onBeforeGetResourceList.bind(this));
		}
	},

	// Map thread only
	onBeforeGetComponents: function (weDontCare) {
		this.onBeforeGetResourceList([]);
	},

	// Runs on main thread only
	onBeforeGetResourceList: function (list) {
		/* Sheet Object
		{
			id: 'mod.some.id.for.sheet'
			path: 'some/path/to/image',
			width: 8 // how many tiles per row
		}
		*/
		let sheets = [];
		this.events.emit('asset-atlas:onGetSpritesheets', sheets);

		let seen = [];
		this.spritesheets = {};
		sheets.forEach(function (sheet) {
			if (!sheet.path) {
				// Must have a path
				throw new SpritesheetMissingPathException(sheet.id);
			}
			if (!sheet.width) {
				// Must have a width, in tiles
				throw new SpritesheetMissingWidthException(sheet.id);
			}

			if (seen.includes(sheet.id)) {
				// Can't have two with the same name
				throw new GenericConflictException('spritesheet ID', sheet.id);
			} else {
				// Add to list so that we track duplicates
				seen.push(sheet.id);
				this.spritesheets[sheet.id] = sheet;
			}
		}, this);

		// Load provided sheets on client
		list = [].concat(list, sheets.map(function (sheetConfig) {
			return sheetConfig.path;
		}));

		/*
		{
			id: 'mod.some.id.for.asset',
			cell: number OR
			position: [#, #],
			sheet: str
		}
		*/
		let mappings = [];
		this.events.emit('asset-atlas:onGetMappings', mappings);
		seen = [];
		this.mappings = {};
		mappings.forEach(function (mapping) {
			if (!mapping.sheet) {
				// Can't load images without a spritesheet for it
				throw new AssetMissingSpritesheetException(mapping.id);
			}

			if (typeof mapping.cell === 'undefined' && !mapping.position) {
				// Can't determine the image without the image ofc
				throw new AssetMissingSpriteException(mapping.id);
			}
			if (typeof mapping.cell === 'undefined') {
				let cell = this.positionToCell(mapping.position, this.spritesheets[mapping.sheet]);
				mapping.cell = cell;
			}
			if (!mapping.position) {
				let position = this.cellToPosition(mapping.cell, this.spritesheets[mapping.sheet]);
				mapping.position = position;
			}

			if (seen.includes(mapping.id)) {
				// Can't have two with the same name
				throw new GenericConflictException('mapping ID', mapping.id);
			} else {
				// Add to list so that we track duplicates
				seen.push(mapping.id);
				this.mappings[mapping.id] = mapping;
			}
		}, this);

		// Finished loading, emit our methods for other mods to use
		this.events.emit('asset-atlas:onAtlasReady', {
			cellToPosition: this.cellToPosition.bind(this),
			positionToCell: this.positionToCell.bind(this),
			xyToCell: this.xyToCell.bind(this),
			getAsset: this.getAsset.bind(this),
			getAssetSpritesheet: this.getAssetSpritesheet.bind(this),
			getSpritesheet: this.getSpritesheet.bind(this)
		});
	},

	cellToPosition: function (cell, sheet) {
		let x = cell % sheet.width;
		let y = Math.floor(cell / sheet.width);
		return [x, y];
	},
	positionToCell: function (pos, sheet) {
		let x = pos[0];
		let y = pos[1];
		return this.xyToCell(x, y, sheet);
	},
	xyToCell: function (x, y, sheet) {
		let cell = x + (sheet.width * y);
		return cell;
	},

	getAsset: function (id) {
		let asset = this.mappings[id];
		if (!asset)
			throw new UnknownAssetException(id);
		else
			return asset;
	},
	getAssetSpritesheet: function (id) {
		let ss = this.getAsset(id).sheet;
		return this.getSpritesheet(ss);
	},

	getSpritesheet: function (id) {
		let sheet = this.spritesheets[id];
		if (!sheet)
			throw new UnknownAssetException(id + '(sheet)');
		else
			return sheet;
	}
};
